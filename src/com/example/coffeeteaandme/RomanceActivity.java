package com.example.coffeeteaandme;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.Random;

public class RomanceActivity extends ActionBarActivity {
	String male, female, sibling1, sibling2, parent1, parent2;
	int check = 0;
	Random random = new Random();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_romance);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.romance, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void mainmale(View view){
		if (check == 0) {
			Button button = (Button) view;
			male = button.getText().toString();
			view.setVisibility(View.GONE);
			TextView txt = (TextView)findViewById(R.id.text1);
			txt.setText("Choose 1 SUPPORTING CHARACTER");
			check++;
		}
		else {
			Button button = (Button) view;
			parent1 = button.getText().toString();
			setContentView(R.layout.activity_romance1);
			check = 0;
		}
	}
	
	public void mainfemale(View view){
		if (check == 0) {
			Button button = (Button) view;
			female = button.getText().toString();
			view.setVisibility(View.GONE);
			TextView txt = (TextView)findViewById(R.id.text1);
			txt.setText("Choose 3 SUPPORTING CHARACTER");
			check++;
		}
		else if (check == 1) {
			Button button = (Button) view;
			parent2 = button.getText().toString();
			view.setVisibility(View.GONE);
			TextView txt = (TextView)findViewById(R.id.text1);
			txt.setText("Choose 2 SUPPORTING CHARACTER");
			check++;
		}
		else if (check == 2) {
			Button button = (Button) view;
			sibling1 = button.getText().toString();
			view.setVisibility(View.GONE);
			TextView txt = (TextView)findViewById(R.id.text1);
			txt.setText("Choose 1 SUPPORTING CHARACTER");
			check++;	
		}
		else {
			Button button = (Button) view;
			sibling2 = button.getText().toString();
			setContentView(R.layout.activity_romance2);
			check = 0;
		}
	}
	
	public void nextpage(View view){
		if (check == 0) {
			Button button = (Button) view;
			button.setText("NEXT");
			TextView txt = (TextView)findViewById(R.id.text1);
			txt.setTextSize(25);
			int randomInt = random.nextInt(2);
			
			if(randomInt == 0) {
				txt.setText("It was the 14th of February and " + male + "�s grandfather�s 80th birthday; so " + male + 
				"�s parents threw a grand party in the grass patch next to their bungalow. They invited all of their family " +
				"and friends for a grand barbecue. Imagine a garden, candles everywhere, great music and lots of happy people. " +
				male + " was barbecuing his chicken wings, when he glanced upon this beautiful girl, " + female + ". Their eyes met " +
				"each other. They smiled. A few minutes later, " + female + " joined " + male + " at the barbecue pit and " +
				"starting cooking some marshmallows and corn. They exchanged a casual conversation and parted. After the party, " +
				male + " couldn�t stop thinking about " + female + ".");
			}
			else {
				txt.setText("It was the first day of school; " + male + " watched the morning sun light up his room and " +
				"realised how late he was for school. He quickly got up from his bed and started getting ready. He grabbed a " +
				"small piece of bread to eat on the way to school. After a short 20 min walk, " + male + " was relieved that " +
				"he had finally made it to school. He heard the class bell ringing and rushed to find his new classroom. As he " +
				"rushed to open the classroom door, he bumped into " + female + ". " + male + " helped " + female + " pick up " +
				"all her notes. " + male + " realised " + female + " was none other than his crush for the past 3 years. " + male
				+ " used to secretly admire " + female + " but never had the guts to approach her. When " + female + " offered " +
				"to treat " + male + " to coffee for the help and he gladly accepted it. After school they went out for coffee.");
			}
			
			check++;
		}
		else if (check == 1) {
			TextView txt = (TextView)findViewById(R.id.text1);
			int randomInt = random.nextInt(2);
			
			if(randomInt == 0) {
				txt.setText("Over the next few months, " + male + " and " + female + " met up several times for coffee and meals. " +
				"On New Year�s Eve, " + male + " asked " + female + " to go out for a movie. She gladly agreed. They went " +
				"to watch Titanic. The movie started, both of them enjoyed the fun parts. During the climax, " + female + 
				" couldn�t hold her tears and started crying. " + male + " lent " + female + " his shoulder and both continued " +
				"watching the tragic love story of Jack and Rose. After the movie, " + male + " and " + female + " went to a " +
				"nearby restaurant for dinner. After dinner, they decided to go for a small walk before heading home. They were " +
				"discussing all their favorite parts of Titanic while walking; just then, their hands touched. They looked at " +
				"each other for few seconds, smiled and then held each other�s hands. As the struck 12, they witnessed the " +
				"spectacular fireworks and then shared a kiss.");
			}
			else {
				txt.setText("Over the next few months, " + male + " and " + female + " met up regularly during school breaks. " +
				"When the holidays started, both of them decided to spend a day together at the theme park. They walked around " +
				"and took pictures with life-size cartoon characters and ate their favorite snacks. " + male + " insisted on " +
				"trying out the scariest rollercoaster in the park. " + female + " was slightly scared of the rollercoaster but " +
				"decided to give it a shot. " + male + " sensed how scared " + female + " was and held her hand throughout " +
				"their ride. She thought that was very thoughtful of him trying to protect her. At the end, they decided to ride " +
				"the ferris wheel. When they reached the top of the wheel, they watched the beautiful sunset and confessed their " +
				"feelings for each other.");
			}
			
			check++;
		}
		else if (check == 2) {
			TextView txt = (TextView)findViewById(R.id.text1);
			int randomInt = random.nextInt(2);
			
			if(randomInt == 0) {
				txt.setText("One day " + male + " went shopping with his sister, " + sibling1 + ". At the mall, they were checking " +
				"out a few end-of-season sales. " + sibling1 + " noticed this beautiful red pair of shoes. She went to grab the last " +
				"pair but there was someone else who wanted the same pair of shoes. " + sibling1 + " then started arguing with that " +
				"other person about who grabbed the shoes first. In the midst of the heated argument, " + female + " had arrived. " + 
				male + " introduced " + female + " as his girlfriend to " + sibling1 + ". " + female + " introduced the other girl " +
				"as her sister, " + sibling2 + ". Everyone was shocked for a moment, then " + sibling1 + " and " + sibling2 + 
				" continued to fight for the pair of shoes. " + sibling1 + " and " + sibling2 + " hated each other and decided to ban "
				+ male + " and " + female + " from dating each other.");
			}
			else {
				txt.setText(male + " wanted to introduce " + female + " to his sister " + sibling1 + " so he brought " 
				+ female + " and " + sibling1 + " out for coffee one day. " + male + " held a lot of respect for " + sibling1 + 
				" it was really important for " + male + " that " + sibling1 + " likes " + female + ". They had a great conversation " +
				"and everything seemed to go well until " + sibling2 + " appeared. " + sibling2 + " is " + female + "�s elder sister. " +
				sibling2 + " and " + sibling1 + " went to high school together. They were each other's competitors in school and hated each " +
				"other because of that. As " + sibling2 + " entered the cafe, the first person she saw was " + sibling1 + ". " + 
				sibling1 + " stood up and both of them started arguing with each other. " + sibling2 + " then found out that " + female +
				" was there too and she was dating " + male + " who is " + sibling1 + "�s younger brother! The argument ended with " + 
				sibling1 + " splashing a cup of water at " + sibling2 + "�s face. Both " + sibling1 + " and " + sibling2 + 
				" subsequently banned their siblings from dating each other.");
			}
			
			check++;	
		}
		else if (check == 3) {
			TextView txt = (TextView)findViewById(R.id.text1);
			int randomInt = random.nextInt(2);
			
			if(randomInt == 0) {
				txt.setText("After a lot of convincing, " + sibling1 + " finally agreed to let " + male + " date " + female + ". " + 
				male + " was felt relieved. But " + sibling2 + " wouldn�t allow " + female + " to date "
				+ male + ". " + female + " was all sad and gloomy, so after some time, " + female + "�s parents " + parent1 + " and " +
				parent2 + " decided to " + "intervene. " + parent1 + " and " + parent2 + " asked " + sibling2 + " to give " + male + 
				" and " + female + "�s relationship one chance. After a long discussion, " + sibling2 + " finally agreed.  " + female + 
				" was so thankful for that, she decided to give " + sibling2 + " a hug.");
			}
			else {
				txt.setText("Over the next few days, " + male + " and " + female + " missed each other a lot and tried convincing their " +
				"families to allow them to date each other. " + male + "�s parents were very open-minded about it and helped " + male + " convince " +
				sibling1 + " and allow " + male + " to date " + female + ". Things did not go that well for " + female + ". She was " +
				"not able to convince her family. She missed " + male + " so much that she had stopped eating and sleeping well. Her " +
				"parents " + parent1 + " and " + parent2 + " noticed her deteriorating health and discussed with " + sibling2 + ". They " +
				"finally agreed on allowing " + female + " to see " + male + ". " + female + " was so happy and thankful for that, she " +
				"decided to give " + sibling2 + " a card.");
			}
			
			check++;	
		}
		else {
			TextView txt = (TextView)findViewById(R.id.text1);
			int randomInt = random.nextInt(2);
			
			if(randomInt == 0) {
				txt.setText(male + " and " + female + " continued dating each other. They got married after a few years and lived happily ever after.");
			}
			else {
				txt.setText(male + " and " + female + " continued dating each other. The love slowly faded away so they decided to break up amicably after 2 years of dating.");
			}
			view.setVisibility(View.GONE);
		}
	}

}

package com.example.coffeeteaandme;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class ThemeActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_theme);
		
		Button horror = (Button) findViewById(R.id.button1);
		horror.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ThemeActivity.this, HorrorActivity.class);
				startActivity(intent);
			}
		});
		
		Button epic = (Button) findViewById(R.id.button2);
		epic.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ThemeActivity.this, EpicActivity.class);
				startActivity(intent);
			}
		});
		
		Button adventure = (Button) findViewById(R.id.button3);
		adventure.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ThemeActivity.this, AdventureActivity.class);
				startActivity(intent);
			}
		});
		
		Button romance = (Button) findViewById(R.id.button4);
		romance.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ThemeActivity.this, RomanceActivity.class);
				startActivity(intent);
			}
		});
		
		Button tragic = (Button) findViewById(R.id.button5);
		tragic.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ThemeActivity.this, TragicActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.theme, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
